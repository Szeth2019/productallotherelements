# This is an alternate solution
# The product of all other elements besides i is the product of everything before and after it
# We can get the products before and after i and then multiply them together

n = [1,2,3,4,5]

def products(nums):
    # generate products starting from the left that include up to i (num is i in this case)
    products_from_left = []
    for num in nums:
        # if array products_from_left is not empty
        if products_from_left:
            products_from_left.append(products_from_left[-1] * num)
        # if array products_from_left is empty
        else:
            products_from_left.append(num)

    print("Products from the left are: " + str(products_from_left))


    # Now I generate the products from the right

    products_from_right = []

    # reverse array to do the same as above but starting from the right side of the array
    # this gives the products including i but from the opposite direction
    # This will be the reverse order of what we want so we have to reverse it after we are done
    
    for num in reversed(nums):
        # if products_from_right not empty
        if products_from_right:
            products_from_right.append(products_from_right[-1] * num)
        # if empty array
        else:
            products_from_right.append(num)

    # reverse array so i = 0 is the product starting from the right of i
    products_from_right = list(reversed(products_from_right))

    print("Products from the right are: " + str(products_from_right))

    final_result = []
    for i in range(len(nums)):
        # if i = 0, products of everything to the right
        if i == 0:
            final_result.append(products_from_right[i+1])
        # if i = last - 1, get product of everything to the left
        elif i == len(nums) - 1:
            final_result.append(products_from_left[i-1])
        # else return product of everything before i and everything after i
        else:
            final_result.append(products_from_left[i-1] * products_from_right[i+1])

    # return the final result
    return final_result


print("The products of all other elements other than i are: " + str(products(n)))


# # This is the original solution
#
# array = [1,2,3,4,5]
#
# def products(nums):
#     prods = 1
#     int(prods)
#     for num in nums:
#         prods = prods * num
#
#
#     arr = []
#
#     for num in nums:
#         arr.append(int(prods/num))
#
#     return arr
#
# answer = products(array)
#
# print(answer)

